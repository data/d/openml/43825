# OpenML dataset: Nutritional-values-for-common-foods-and-products

https://www.openml.org/d/43825

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I found this data occasionally and I just could not pass by. So I hope that this dasatet will help anyone who interested in food nutrition values.
Content
This dataset contains nutrition values for about 8.8k types of food. The features names is very self-explanatory, so I'll not make a description for them.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43825) of an [OpenML dataset](https://www.openml.org/d/43825). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43825/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43825/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43825/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

